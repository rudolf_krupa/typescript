var ProfileSlider = (function () {
    function ProfileSlider(jQuery) {
        this.sliderElements = {
            'slider': '#profilePhoto .slider',
            'controls': {
                'next': '.controls .next',
                'prev': '.controls .prev',
            },
            'statusClass': 'current',
            'status': '.images img.current',
            'firstImage': '.images img:first-child',
            'images': '.images img',
            'imagePaginator': {
                'totalImages': '.pageNumber .totalImages',
                'currentImage': '.pageNumber .currentImage'
            }
        };
        this.updateStatus = function () {
            this.status = this.slider.find(this.sliderElements.status);
        };
        this.getPosition = function (element) {
            return (element.index() + 1);
        };
        this.showOrHideButtons = function () {
            if (this.status.next().index() > -1) {
                this.next.show();
            }
            else {
                this.next.hide();
            }
            if (this.status.prev().index() > -1
                && this.getPosition(this.status.prev()) < this.totalImages) {
                this.prev.show();
            }
            else {
                this.prev.hide();
            }
        };
        this.preloadImages = function (images, order) {
            if (order === void 0) { order = 0; }
            var self = this;
            console.log('preloadImages ' + images.length + " " + order);
            for (var _i = 0, images_1 = images; _i < images_1.length; _i++) {
                var image = images_1[_i];
                var img = new Image();
                img.src = image;
                img.onload = function () {
                    console.log("onload");
                    if ((images.length - 1) > order) {
                    }
                };
                img.onerror = function (event) {
                    console.log(event);
                };
                console.log(image);
            }
        };
        this.jQuery = jQuery;
        this.slider = this.jQuery(this.sliderElements.slider);
        this.next = this.slider.find(this.sliderElements.controls.next);
        this.prev = this.slider.find(this.sliderElements.controls.prev);
        console.log(this.next);
        this.slider.find(this.sliderElements.firstImage)
            .addClass(this.sliderElements.statusClass);
        this.totalImages = this.slider.find(this.sliderElements.images).length;
        this.slider.find(this.sliderElements.imagePaginator.totalImages).text(this.totalImages);
        this.updateStatus();
        this.showOrHideButtons();
        var self = this;
        this.next.on('click', function (event) {
            if (self.status.next().index() > -1) {
                self.status.removeClass(self.sliderElements.statusClass)
                    .next().addClass(self.sliderElements.statusClass);
                self.updateStatus();
                self.updatePaginator();
            }
            self.showOrHideButtons();
            return false;
        });
        this.prev.on('click', function (event) {
            console.log("prev");
            if (self.status.prev().index() > -1) {
                self.status.removeClass(self.sliderElements.statusClass)
                    .prev().addClass(self.sliderElements.statusClass);
                self.updateStatus();
                self.updatePaginator();
            }
            self.showOrHideButtons();
            return false;
        });
    }
    ProfileSlider.prototype.updatePaginator = function () {
        this.slider.find(this.sliderElements.imagePaginator.currentImage)
            .text(this.getPosition(this.status));
    };
    return ProfileSlider;
}());
//# sourceMappingURL=ProfileSlider.js.map