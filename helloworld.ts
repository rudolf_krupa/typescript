'use strict'

class SampleTestParent {
  public name: string;
  public constructor(theName: string) {
    this.name = "before name " + theName;
  }
}

class Test extends SampleTestParent {
    public constructor(theName: string) {
      super(theName);
    }
    public getName(): string {
      return this.name;
    }
}

var test = new Test("fff");
//console.log(test.getName());

////////////////////////////////////////////////////////////////////////////////
class MyDate extends Date {}

var myDate = new MyDate();
//console.log(myDate.toUTCString());
console.log(MyDate.now());

////////////////////////////////////////////////////////////////////////////////


try {
  throw new Error('oops');
} catch(error) {
  console.log(error.message);
}

try {
  try {
    throw new Error('oops');
  }
  finally {
    console.log('finally');
  }
}
catch (ex) {
  console.error('outer', ex.message);
}

////////////////////////////////////////////////////////////////////////////////

/*
//var promise = new Promise();

var promise = new Promise(function(resolve) {
    setTimeout(function() {
        resolve("result");
    }, 1000);
});

promise.then(function(result) {
    alert("Fulfilled: " + result);
}, function(error) {
    alert("Rejected: " + error);
});
*/

////////////////////////////////////////////////////////////////////////////////
