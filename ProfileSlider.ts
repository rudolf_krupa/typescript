
interface jQuery {
    (el: HTMLElement): jQuery;
    (select: string): jQuery;
    find(selector: string): jQuery;
    next(): jQuery;
    prev(): jQuery;
    addClass(className: string): jQuery;
    removeClass(className: string): jQuery;
    text(innerText: string|number): jQuery;
    on(eventName: string, item: object): jQuery;
    index(): number;
    size(): number;
}

interface SliderControlElements {
  next: string;
  prev: string;
}

interface SliderPaginatorElements {
  totalImages: string;
  currentImage: string;
}

interface SliderElements {
  slider: string;
  controls: SliderControlElements;
  statusClass: string;
  status: string;
  firstImage: string;
  images: string;
  imagePaginator: SliderPaginatorElements;

}

class ProfileSlider {

  private jQuery: jQuery;
  private slider: jQuery;
  private next: jQuery;
  private prev: jQuery;

  // image paginator
  public totalImages: number;

  private status: jQuery;

  // view
  public sliderElements: SliderElements = {
    'slider': '#profilePhoto .slider',
    'controls': {
      'next': '.controls .next',
      'prev': '.controls .prev',
    },
    'statusClass': 'current',
    'status': '.images img.current',
    'firstImage': '.images img:first-child',
    'images': '.images img',
    'imagePaginator': {
      'totalImages': '.pageNumber .totalImages',
      'currentImage': '.pageNumber .currentImage'
    }
  };

  constructor(jQuery: jQuery) {

      this.jQuery = jQuery;

      // set variables
      this.slider = this.jQuery(this.sliderElements.slider);

      this.next = this.slider.find(this.sliderElements.controls.next);
      this.prev = this.slider.find(this.sliderElements.controls.prev);

      console.log(this.next);

      // show first
      this.slider.find(this.sliderElements.firstImage)
        .addClass(this.sliderElements.statusClass);

      // set total images for image paginator
      this.totalImages = this.slider.find(this.sliderElements.images).length;
      this.slider.find(this.sliderElements.imagePaginator.totalImages).text(this.totalImages);

      // first run
      this.updateStatus();
      this.showOrHideButtons();

      let self = this;

      // controld
      this.next.on('click', function (event: object):boolean {
          if (self.status.next().index() > -1) {
              self.status.removeClass(self.sliderElements.statusClass)
                  .next().addClass(self.sliderElements.statusClass);

                self.updateStatus();
                self.updatePaginator();
          }

          self.showOrHideButtons();
          return false;
      });

      this.prev.on('click', function (event: object) :boolean {

          console.log("prev");

          if (self.status.prev().index() > -1) {
              self.status.removeClass(self.sliderElements.statusClass)
                  .prev().addClass(self.sliderElements.statusClass);

              self.updateStatus();
              self.updatePaginator();
          }

          self.showOrHideButtons();
          return false;
      });
    }

    updateStatus = function() {
      this.status = this.slider.find(this.sliderElements.status);
    }

    updatePaginator() {
      this.slider.find(this.sliderElements.imagePaginator.currentImage)
        .text(this.getPosition(this.status));
    }

    getPosition = function(element: jQuery):number {
        return (element.index() + 1);
    }

    showOrHideButtons = function():void {
      // next
      if (this.status.next().index() > -1) {
          this.next.show();
      } else {
          this.next.hide();
      }

      // preview
      if (
          this.status.prev().index() > -1
          && this.getPosition(this.status.prev()) < this.totalImages
      ) {
          this.prev.show();
      } else {
          this.prev.hide();
      }
    }



    preloadImages = function(images: Array<string>, order: number = 0):void {
      let self = this;

      console.log('preloadImages ' + images.length + " " + order);

      for(let image of images) {
        let img = new Image();
        img.src = image;
        img.onload = function() {

          console.log("onload");

          if((images.length - 1) > order) {
            //self.preloadImages(images, order + 1);
          }

        }
        img.onerror = function(event){
          console.log(event);

          /*
          if((images.length - 1) > order) {
            self.preloadImages(images, order + 1);
          }
          */

        }

        console.log(image);


      }
    }
}
