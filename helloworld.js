'use strict';
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var SampleTestParent = (function () {
    function SampleTestParent(theName) {
        this.name = "before name " + theName;
    }
    return SampleTestParent;
}());
var Test = (function (_super) {
    __extends(Test, _super);
    function Test(theName) {
        return _super.call(this, theName) || this;
    }
    Test.prototype.getName = function () {
        return this.name;
    };
    return Test;
}(SampleTestParent));
var test = new Test("fff");
var MyDate = (function (_super) {
    __extends(MyDate, _super);
    function MyDate() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return MyDate;
}(Date));
var myDate = new MyDate();
console.log(MyDate.now());
try {
    throw new Error('oops');
}
catch (error) {
    console.log(error.message);
}
try {
    try {
        throw new Error('oops');
    }
    finally {
        console.log('finally');
    }
}
catch (ex) {
    console.error('outer', ex.message);
}
//# sourceMappingURL=helloworld.js.map